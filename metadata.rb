name             "IaaS Benchmarking Suite"
maintainer       "Nane Kratzke"
maintainer_email "nane.kratzke@fh-luebeck.de"
license          "GPL somewhat"
description      "Used by the Fast Start guides at wiki.opscode.com"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.1"
