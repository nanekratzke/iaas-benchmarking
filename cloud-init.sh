#! /bin/bash

# Setting up some meta data about the node and S3 connection data
# to store the benchmark results.
export AWS_INSTANCE_TYPE="<instance_type>"
export AWS_REGION="<region>"

# Define S3 connection data for uploading benchmark results
export S3_ACCESS_KEY="<your access key>"
export S3_SECRET="<your secret access key>"
export S3_BUCKET="<bucket to store benchmark results>"

# Install chef configuration management system from source.
curl -L https://www.opscode.com/chef/install.sh | sudo bash

# Install some basic packages
aptitude update
aptitude -y install language-pack-de
aptitude -y install git

# Clone necessary repositories for benchmarking.
mkdir /var/chef
mkdir /var/chef/cookbooks

cd /var/chef/cookbooks
git clone https://nanekratzke@bitbucket.org/nanekratzke/iaas-benchmarking.git

# Install the benchmark
chef-solo -j /var/chef/cookbooks/iaas-benchmarking/install.json