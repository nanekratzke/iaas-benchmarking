# Upload configuration
node.default[:aws][:access_key] = ENV['S3_ACCESS_KEY']
node.default[:aws][:secret] = ENV['S3_SECRET']
node.default[:bucket] = ENV['S3_BUCKET']

# Node description
node.default[:region] = ENV['AWS_REGION']
node.default[:itype] = ENV['AWS_INSTANCE_TYPE']

# Benchmarks to run
node.default[:benchmarks] = [
	"compress-pbzip2",
	"scimark2",
	"byte",
	"stream",
	"iozone",
	"sqlite",
	"network-loopback"
]