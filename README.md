IaaS Benchmark
==============

This chef cookbook is used to install a Phoronix based IaaS benchmarking suite on a IaaS node.

It is tested on Ubuntu 12.04 LTS systems. Other systems may work, but are not tested.

This benchmarking suite can be used to measure and compare the performance of IaaS
processing nodes.

The benchmark suite contains the following benchmarks:

+ SciMark2 (Dense LU Matrix Factorization) to measure numerical processing performance
+ Drhystone 2 to measure integer processing performance
+ Parallel BZIP2 to measure parallel processing performance
+ Stream (copy) to measure read/write memory bandwidths
+ IOZone to measure read/write disk performance
+ SQLite to measure database access disk performance
+ Network loopback to measure node responsible network (TCP) performance

How to install?
---------------


How is it working?
------------------

This cookbook provides the following recipes

+ install_benchmark to install the phoronix test suite and the above mentioned benchmarks
+ run_benchmark to execute the installed and configured benchmarks
+ upload_results to upload measured benchmark result files to a S3 destination


How long is a benchmark running?
-----------------------

Well this depends. But the benchmark is designed that a complete benchmark run needs
approximately one hour to finish on small instance types  and is faster on more 
powerful instances. The above mentioned benchmarks are
designed to generate test results covering various performance aspects in less than one
hour which is normally the smallest purchasing unit in IaaS billing. So this test suite
will not waste money on 1 hour and 1 minute test runs ...