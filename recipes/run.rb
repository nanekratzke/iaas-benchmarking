#
# Cookbook Name:: IaaS Bench
# Recipe:: run_benchmark
#
# Copyright 2013, Nane Kratzke
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Stop cron services in order to avoid benchmarking influence 
# of background services
service "cron" do
	action :stop
end

service "anacron" do
	action :stop
end

# Run the installed benchmarks
for bm in node[:benchmarks] do
	execute bm do
  		command "phoronix-test-suite batch-benchmark #{bm}"
  		action :run
	end
end

# Restart cron services
service "cron" do
	action :start
end

service "anacron" do
	action :start
end