#
# Cookbook Name:: IaaS Bench
# Recipe:: install_benchmark
#
# Copyright 2013, Nane Kratzke
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Update package lists (necessary for some installs)
execute "update package list" do
	command "aptitude update"
	action :run
end

# Install s3cmd to provide benchmark results upload capability to s3
package "s3cmd"
template "/root/.s3cfg" do
  source "s3cfg.erb"
  mode 0440
  owner "root"
  group "root"
end

# Install upload script
template "/root/upload_results.sh" do
	source "upload_results.erb"
	mode 0755
	owner "root"
	group "root"
end

# Install phoronix benchmarking suite (and dependencies)
package "phoronix-test-suite"

# Install the following benchmarks
# - scimark2 (Processing, numerical)
# - byte (Processing, Drhystone 2, integer/string processing)
# - compress bzip2 (Parallel processing)
# - stream (Memory, bandwidth)
# - iozone (I/O, read and write performance)
# - sqlite (I/O, db file operations)
# - network loopback (Node TCP performance)
for bm in node[:benchmarks] do
	execute bm do
  		command "phoronix-test-suite install #{bm}"
  		action :run
	end
end

# Configure general benchmark settings
cookbook_file "/root/.phoronix-test-suite/user-config.xml" do
  source "user-config.xml"
  mode 00644
end

# Configure compress-pbzip2 benchmark (nothing to do)

# Configure SciMark2 benchmark (Dense LU Matrix Factorization)
cookbook_file "/root/.phoronix-test-suite/test-profiles/pts/scimark2-1.1.1/test-definition.xml" do
  source "scimark2-test-definition.xml"
  mode 00644
end

# Configure Byte benchmark (Dryhstone 2)
cookbook_file "/root/.phoronix-test-suite/test-profiles/pts/byte-1.2.0/test-definition.xml" do
  source "byte-test-definition.xml"
  mode 00644
end

# Configure Stream benchmark (copy)
cookbook_file "/root/.phoronix-test-suite/test-profiles/pts/stream-1.1.0/test-definition.xml" do
  source "stream-test-definition.xml"
  mode 00644
end

# Configure IOZone benchmark (1 MB records, 2 GB file, read and write)
cookbook_file "/root/.phoronix-test-suite/test-profiles/pts/iozone-1.8.0/test-definition.xml" do
  source "iozone-test-definition.xml"
  mode 00644
end

# Configure SQLite benchmark (nothing to do)

# Configure Network loopback benchmark (nothing to do)